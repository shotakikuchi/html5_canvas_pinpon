window.onload = function(){

	var canvas,ctx,myPaddle,mouseX,myBall,score,scoreLabel;
	var isPlaying = false;
	var timerId;


	canvas = document.getElementById('canvas');
	//canvasに対応してなかったらそこで処理を止める。
	if(! canvas || ! canvas.getContext) return false;

	ctx = canvas.getContext('2d');

	function init(){
		//PaddleオブジェクトをmyPaddleに格納する
		myPaddle = new Paddle(100,10);
		myBall = new Ball(rand(10,100),rand(10,100),5,rand(3,8),rand(3,8));
		//ScoreオブジェクトをscoreLabelに格納する
		scoreLabel = new Score(10,20);
		score = 0;
		isPlaying = true;
	}

	//高さと幅を渡してパドルを作れるオブジェクトを作成する
	var Paddle = function(w,h){
		this.w = w;
		this.h = h;
		this.x = canvas.width / 2;
		this.y = canvas.height - 30;
		this.draw = function(){
			ctx.fillStyle = "#00aaff";
			//バーの半分だけ左にずらす。結果中央揃えになる。
			ctx.fillRect(this.x - this.w / 2,this.y,this.w,this.h);
		};

		//this.xをマウスのx座標からcanvaxの左のx座表を引いたものに設定。
		this.move = function(){
			this.x = mouseX - $("#canvas").offset().left;
		}
	}

	//ボールオブジェクト
	var Ball = function(x,y,r,vx,vy){
		this.x = x;
		this.y = y;
		this.r = r;
		this.vx = vx;
		this.vy = vy;

		//ボールを描くメソッド
		this.draw = function(){
			ctx.beginPath();
			ctx.fillStyle = "#00ff00";
			ctx.arc(this.x,this.y,this.r,0,2*Math.PI);
			ctx.closePath();
			ctx.fill();
			//console.log('log');
		};

		//ボールのうごきを制御するメソッド
		this.move = function(){

			//左端 or 右端
			if(this.x + this.r > canvas.width || this.x - this.r < 0){
				this.vx *= -1;
			}
			//上端
			if(this.y - this.r < 0){
				this.vy *= -1;
			}
			//下端(geme over)
			if(this.y + this.r > canvas.height){
				isPlaying = false;
				$('#btn').text("REPLAY?").fadeIn();
			}
			this.x += this.vx;
			this.y += this.vy;
		};

		//パドルとの衝突判定
		this.checkCollision = function(paddle){
			if((this.y + this.r > paddle.y - paddle.h / 2) && (this.x > paddle.x - paddle.w / 2 && this.x <  paddle.x + paddle.w / 2)){
				this.vy *= -1.01;
				score++;
				if(score % 3 == 0) {
					this.vx = rand(-10,30);
					this.vy *= 1.2;
					myPaddle.w *= 0.8;
				}
			}
		}
	}

	//スコアオブジェクト
	var Score = function(x,y){
		this.x = x;
		this.y = y;
		this.draw = function(text){
			ctx.font = 'bold 14px "Century Gothic"';
			ctx.fillStyle = "#00aaff";
			ctx.textAlign = "left";
			ctx.fillText(text,this.x,this.y);
		}
	}

	//0.03秒ごとに実行
	function update(){
		clearStage();

		myPaddle.move();
		myBall.move();
		//Paddle内のメソッド .draw()で描画する。
		myPaddle.draw();
		myBall.draw();
		//パドルとの衝突判定
		myBall.checkCollision(myPaddle);
		//scoreを描画する
		scoreLabel.draw("SCORE : " + score);
		timerId = setTimeout(function(){
			update();
		},30);

		if(!isPlaying) clearTimeout(timerId);
	}

	$("#btn").click(function(){
		$(this).fadeOut();
		init();
		update();
	});



	//背景色で描画する。
	function clearStage(){
		ctx.fillStyle = "#aaedff";
		ctx.fillRect(0,0,canvas.width,canvas.height);
	}

	//マウスが動くたびに実行
	$('body').mousemove(function(e){
		mouseX = e.pageX;
	});

	//指定した範囲の乱数を表示させる関数
	function rand(min,max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

}
